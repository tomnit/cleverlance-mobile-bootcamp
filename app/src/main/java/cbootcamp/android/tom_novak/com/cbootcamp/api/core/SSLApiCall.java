package cbootcamp.android.tom_novak.com.cbootcamp.api.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

/**
 * Created by tom on 29.9.15.
 */
public abstract class SSLApiCall<T, S> {

    private static final int READ_TIMEOUT = 10000;
    private static final int CONNECT_TIMEOUT = 15000;

    public S doExecute(T param) throws SimpleStupidException {
        try {
            // open & prepare url connection
            URL url = new URL(getUrl());
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setReadTimeout(getReadTimeout());
            urlConnection.setConnectTimeout(getConnectTimeout());
            urlConnection.setRequestMethod(getRequestMethod());

            // Create the SSL connection
            SSLContext sc;
            sc = SSLContext.getInstance("TLS");
            sc.init(null, null, new java.security.SecureRandom());
            urlConnection.setSSLSocketFactory(sc.getSocketFactory());

            // set content type
            urlConnection.setRequestProperty("Content-Type", getContentType());

            // set other request properties
            Map<String, String> requestProperties = new HashMap<>();
            appendRequestProperties(requestProperties);
            for (String key : requestProperties.keySet()) {
                urlConnection.setRequestProperty(key, requestProperties.get(key));
            }

            urlConnection.setDoInput(true);

            // write data to request
            urlConnection.setDoOutput(hasRequestBody());
            if (hasRequestBody()) {
                writeRequestData(urlConnection.getOutputStream());
            }

            // send request
            urlConnection.connect();

            // response evaluation
            int responseCode = urlConnection.getResponseCode();

            if(responseCode == 200) {
                return readResponseData(urlConnection.getInputStream());
            } else {
                throw new SimpleStupidException();
            }
        } catch (NoSuchAlgorithmException e) {
            throw new SimpleStupidException();
        } catch (KeyManagementException e) {
            throw new SimpleStupidException();
        } catch (ProtocolException e) {
            throw new SimpleStupidException();
        } catch (MalformedURLException e) {
            throw new SimpleStupidException();
        } catch (IOException e) {
            throw new SimpleStupidException();
        }
    }

    protected String getRequestMethod() {
        return "GET";
    }

    protected int getReadTimeout() {
        return READ_TIMEOUT;
    }

    protected int getConnectTimeout() {
        return CONNECT_TIMEOUT;
    }

    protected boolean hasRequestBody() {
        return false;
    }

    protected abstract String getUrl();

    protected abstract void appendRequestProperties(Map<String, String> requestProperties);

    protected abstract String getContentType();

    protected abstract void writeRequestData(OutputStream os);

    protected abstract S readResponseData(InputStream is);
}
