package cbootcamp.android.tom_novak.com.cbootcamp.api.core;

/**
 * Created by tom on 29.9.15.
 */
public interface IApiCall<T, S> {

    S execute(T param) throws SimpleStupidException;
}
