package cbootcamp.android.tom_novak.com.cbootcamp.api;

import android.util.Base64;
import android.util.Base64InputStream;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import cbootcamp.android.tom_novak.com.cbootcamp.api.core.AuthApiCall;
import cbootcamp.android.tom_novak.com.cbootcamp.api.core.IAuthApiCall;
import cbootcamp.android.tom_novak.com.cbootcamp.api.core.SimpleStupidException;
import cbootcamp.android.tom_novak.com.cbootcamp.domain.LoginData;

/**
 * Created by tom on 29.9.15.
 */
public class DownloadImageApiCall extends AuthApiCall<String, Base64InputStream> implements IAuthApiCall<String, Base64InputStream> {

    private final String REQUEST_METHOD = "POST";
    private final String CONTENT_TYPE = "application/x-www-form-urlencoded";

    @Override
    public Base64InputStream execute(LoginData auth, String param) throws SimpleStupidException {
        return super.doExecute(auth, param);
    }

    @Override
    protected String getUrl() {
        return ApiUrl.HOST + ApiUrl.GET_IMAGE;
    }

    @Override
    protected String getContentType() {
        return CONTENT_TYPE;
    }

    @Override
    protected String getRequestMethod() {
        return REQUEST_METHOD;
    }

    @Override
    protected void writeRequestData(OutputStream os) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write("username=" + auth.getUsername());
            writer.flush();
            writer.close();
            os.close();
        } catch (UnsupportedEncodingException e) {
            throw new SimpleStupidException();
        } catch (IOException e) {
            throw new SimpleStupidException();
        }
    }

    @Override
    protected Base64InputStream readResponseData(InputStream is) {
        return new Base64InputStream(is, Base64.URL_SAFE);
    }

    @Override
    protected boolean hasRequestBody() {
        return true;
    }
}
