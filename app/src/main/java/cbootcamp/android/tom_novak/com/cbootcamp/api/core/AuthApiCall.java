package cbootcamp.android.tom_novak.com.cbootcamp.api.core;

import java.util.Map;

import cbootcamp.android.tom_novak.com.cbootcamp.domain.LoginData;

/**
 * Created by tom on 29.9.15.
 */
public abstract class AuthApiCall<T, S> extends SSLApiCall<T, S> {

    protected LoginData auth;

    protected S doExecute(LoginData auth, T param) throws SimpleStupidException {
        this.auth = auth;
        S retVal = super.doExecute(param);
        auth = null;
        return retVal;
    }

    @Override
    protected void appendRequestProperties(Map<String, String> requestProperties) {
        if(auth != null) {
            requestProperties.put("Authorization", auth.getPasswordSHA1());
        }
    }
}
