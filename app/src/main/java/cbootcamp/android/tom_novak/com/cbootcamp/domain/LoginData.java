package cbootcamp.android.tom_novak.com.cbootcamp.domain;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import cbootcamp.android.tom_novak.com.cbootcamp.util.Utils;

/**
 * Created by tom on 18.9.15.
 */
public class LoginData {

    public static final String TAG = "cbootcamp.LoginData";

    private final String username;
    private final String password;

    public LoginData(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPasswordSHA1() {
        String encodedPassword = null;
        try {
            encodedPassword = Utils.toSHA1(password);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Cannot encode password to SHA1", e);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Cannot encode password to SHA1", e);
        }
        return encodedPassword;
    }
}
