package cbootcamp.android.tom_novak.com.cbootcamp.api.core;

import cbootcamp.android.tom_novak.com.cbootcamp.domain.LoginData;

/**
 * Created by tom on 29.9.15.
 */
public interface IAuthApiCall<T, S> {

    S execute(LoginData auth, T param) throws SimpleStupidException;
}
