package cbootcamp.android.tom_novak.com.cbootcamp.api;

/**
 * Created by tom on 18.9.15.
 */
public class ApiUrl {

    public static final String HOST = "https://mobility.cleverlance.com";
    public static final String GET_IMAGE = "/download/bootcamp/image.php";

    private ApiUrl() {}
}
