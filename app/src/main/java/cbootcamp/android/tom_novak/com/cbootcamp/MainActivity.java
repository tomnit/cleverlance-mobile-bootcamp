package cbootcamp.android.tom_novak.com.cbootcamp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64InputStream;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import cbootcamp.android.tom_novak.com.cbootcamp.api.DownloadImageApiCall;
import cbootcamp.android.tom_novak.com.cbootcamp.api.core.IAuthApiCall;
import cbootcamp.android.tom_novak.com.cbootcamp.api.core.SimpleStupidException;
import cbootcamp.android.tom_novak.com.cbootcamp.domain.LoginData;
import cbootcamp.android.tom_novak.com.cbootcamp.util.Utils;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = "cbootcamp.MainActivity";

    public static final int PREVIEW_SIZE = 640;
    public static final String DOWNLOADED_FILE_NAME = "downloadedImage.jpg";
    public static final String appFolderPath = Utils.getAppFolder().getAbsolutePath();


    private EditText usernameView;
    private EditText passwordView;
    private Button downloadBtn;
    private TextView alterImageTextView;
    private ImageView imageView;

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // do nothing
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // do nothing
        }

        @Override
        public void afterTextChanged(Editable s) {
            updateDownloadBtnPossibility();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        if(savedInstanceState != null) {
            //imageView.setImageBitmap(Utils);
        }

        updateDownloadBtnPossibility();
    }

    @Override
    public void onClick(View v) {
        DownloadImageRemoteCall remoteCall = new DownloadImageRemoteCall();
        setAlterTextVisibility(true);
        remoteCall.execute(getValidatedFormData());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("loadLastImage", true);
    }

    protected void initViews() {
        usernameView = (EditText) findViewById(R.id.main_username);
        passwordView = (EditText) findViewById(R.id.main_password);
        downloadBtn = (Button)findViewById(R.id.btn_main_login);
        alterImageTextView = (TextView)findViewById(R.id.main_result_alt_text);
        imageView = (ImageView)findViewById(R.id.main_result_image);

        usernameView.addTextChangedListener(textWatcher);
        passwordView.addTextChangedListener(textWatcher);
        downloadBtn.setOnClickListener(this);
        setAlterTextVisibility(true);
    }

    private LoginData getFormData() {
        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        return new LoginData(username, password);
    }

    private LoginData getValidatedFormData() {
        boolean result = true;

        LoginData loginData = getFormData();

        result = result && (loginData.getUsername() != null && !loginData.getUsername().equals(""));
        result = result && (loginData.getPassword() != null && !loginData.getPassword().equals(""));

        if(result) {
            return loginData;
        } else {
            return null;
        }
    }

    private void updateDownloadBtnPossibility() {
        if(getValidatedFormData() != null) {
            downloadBtn.setEnabled(true);
        } else {
            downloadBtn.setEnabled(false);
        }
    }

    private void setAlterTextVisibility(boolean showAlterText) {
        if(showAlterText) {
            alterImageTextView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
        } else {
            alterImageTextView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
        }
    }

    private void processTaskResult(DownloadImageRemoteCall.TaskResult<Bitmap> taskResult) {
        if(taskResult.errorMessageResource != 0) {
            Toast.makeText(this, taskResult.errorMessageResource, Toast.LENGTH_LONG).show();
            imageView.setImageResource(R.mipmap.bootcamp_launcher);
            setAlterTextVisibility(false);
        } else {
            imageView.setImageBitmap(taskResult.result);
            setAlterTextVisibility(false);
        }
    }

    private class DownloadImageRemoteCall extends AsyncTask<LoginData, Void, DownloadImageRemoteCall.TaskResult<Bitmap>> {

        public class TaskResult<S> {
            public final int errorMessageResource;
            public final S result;

            public TaskResult(int errorMessageResource, S result) {
                this.errorMessageResource = errorMessageResource;
                this.result = result;
            }
        }

        private IAuthApiCall<String, Base64InputStream> apiCall = new DownloadImageApiCall();

        @Override
        protected TaskResult<Bitmap> doInBackground(LoginData... params) {
            if(params.length > 0) {
                try {
                    // get image from web
                    Base64InputStream imageDataStream = apiCall.execute(params[0], null);

                    // save image to SD card
                    Utils.saveFile(imageDataStream, appFolderPath, File.separator + DOWNLOADED_FILE_NAME);

                    // load image thumbnail
                    String imagePath = appFolderPath + File.separator + DOWNLOADED_FILE_NAME;
                    Bitmap thumbnail = Utils.loadImageThumbnail(imagePath, PREVIEW_SIZE, PREVIEW_SIZE);
                    if(thumbnail != null) {
                        return new TaskResult<>(0, thumbnail);
                    } else {
                        return new TaskResult<Bitmap>(R.string.err_cannot_show_image, null);
                    }
                } catch (SimpleStupidException e) {
                    Log.e(TAG, getResources().getString(R.string.err_cannot_download_image), e);
                    return new TaskResult<>(R.string.err_cannot_download_image, null);

                } catch (FileNotFoundException e) {
                    Log.e(TAG, getResources().getString(R.string.err_cannot_show_image), e);
                    return new TaskResult<>(R.string.err_cannot_show_image, null);

                }  catch (IOException e) {
                    Log.e(TAG, getResources().getString(R.string.err_cannot_save_file), e);
                    return new TaskResult<>(R.string.err_cannot_save_file, null);
                }
            } else {
                return new TaskResult<>(R.string.err_application_error, null);
            }
        }

        @Override
        protected void onPostExecute(TaskResult<Bitmap> bitmapTaskResult) {
            processTaskResult(bitmapTaskResult);
        }
    }

    private class LoadImageThumbnailAsyncTask extends AsyncTask<String, Void, Bitmap> {

        public static final String TAG = "cbootcamp.LoadImageTask";

        private final WeakReference<ImageView> imageViewReference;

        public LoadImageThumbnailAsyncTask(ImageView imageView) {
            this.imageViewReference = new WeakReference<ImageView>(imageView);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            if(params.length > 0) {
                FileInputStream fileInputStream = null;
                try {
                    fileInputStream = new FileInputStream(params[0]);
                    Bitmap imageBitmap = BitmapFactory.decodeStream(fileInputStream);
                    if (imageBitmap != null) {
                        return Bitmap.createScaledBitmap(imageBitmap, PREVIEW_SIZE, PREVIEW_SIZE, false);
                    }
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "File not found", e);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if(imageViewReference != null && bitmap != null) {
                ImageView imageView = imageViewReference.get();
                if(imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            } else {
                Toast.makeText(
                        MainActivity.this,
                        getResources().getString(R.string.err_cannot_show_image),
                        Toast.LENGTH_LONG)
                        .show();
            }
        }
    }
}
