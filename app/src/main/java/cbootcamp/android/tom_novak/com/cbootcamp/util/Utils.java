package cbootcamp.android.tom_novak.com.cbootcamp.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by tom on 18.9.15.
 */
public class Utils {

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String toSHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }

    public static void copyStream(InputStream input, OutputStream output) throws IOException {
        byte [] buffer = new byte[256];
        int bytesRead = 0;
        while((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static File getAppFolder() {
        String externalStorageState = Environment.getExternalStorageState();
        if(externalStorageState.equals(Environment.MEDIA_MOUNTED) ||
                externalStorageState.equals(Environment.MEDIA_SHARED) ||
                externalStorageState.equals(Environment.MEDIA_UNMOUNTABLE)) {
            File appFolder = new File(Environment.getExternalStorageDirectory() + "/CBootcamp");
            if(appFolder.exists() && appFolder.isDirectory()) {
                if(appFolder.canWrite()) {
                    return appFolder;
                }
            } else {
                if(appFolder.mkdir()) {
                    return appFolder;
                }
            }
        }
        return null;
    }

    public static void saveFile(InputStream input, String folderPath, String fileName) throws IOException {
        FileOutputStream fos = null;
        try {
            if (input != null) {
                File imageFile = new File(folderPath, fileName);
                fos = new FileOutputStream(imageFile);
                //fos = MainActivity.this.openFileOutput("downloadedImage.jpg", Context.MODE_PRIVATE);
                Utils.copyStream(input, fos);

                fos.flush();
                fos.close();
                input.close();
            }
        } finally {
            if (fos != null) {
                fos = null;
            }
        }
    }

    public static Bitmap loadImageThumbnail(String imagePath, int thumbnailWidth, int thumbnailHeight)
            throws FileNotFoundException {
        FileInputStream fileInputStream
                = new FileInputStream(imagePath);
        Bitmap imageBitmap = BitmapFactory.decodeStream(fileInputStream);
        if(imageBitmap != null) {
            return Bitmap.createScaledBitmap(imageBitmap, thumbnailWidth, thumbnailHeight, false);
        }
        return null;
    }

    private Utils() {}
}
